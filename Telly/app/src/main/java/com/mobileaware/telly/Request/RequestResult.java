package com.mobileaware.telly.Request;

import java.io.Serializable;

import com.android.volley.VolleyError;

public class RequestResult<D> implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -84356001714713132L;
	
	private int requestId;
	private boolean isRequestFailed = true;
	private String errorMessage;
	private RequestStatus requestStatus;
	private D requestResponse;
	private VolleyError error;
	
	public RequestResult()
	{
		
	}
	
	public RequestResult(int a_RequestId)
	{
		requestId = a_RequestId;
	}
	
	public int getRequestId()
	{
		return requestId;
	}
	
	public void setRequestId(int requestId)
	{
		this.requestId = requestId;
	}
	
	public boolean isRequestFailed()
	{
		return isRequestFailed;
	}
	
	public void setRequestFailed(boolean isRequestFailed)
	{
		this.isRequestFailed = isRequestFailed;
	}
	
	public String getErrorMessage()
	{
		return errorMessage;
	}
	
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}
	
	public RequestStatus getRequestStatus()
	{
		return requestStatus;
	}

	public void setRequestStatus(RequestStatus requestStatus)
	{
		this.requestStatus = requestStatus;
	}

	public D getRequestResponse()
	{
		return requestResponse;
	}
	
	public void setRequestResponse(D requestResponse)
	{
		this.requestResponse = requestResponse;
	}

	public VolleyError getError()
	{
		return error;
	}

	public void setError(VolleyError error)
	{
		this.error = error;
	}
}
