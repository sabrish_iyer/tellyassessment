package com.mobileaware.telly.Request;

import android.content.Context;

import com.mobileaware.telly.Utils.UrlConstants;
import com.mobileaware.telly.pojo.TrendingShow;

/**
 * Created by sabri on 5/11/2016.
 */
public class GetTrendingShowRequest  extends BaseRequest<TrendingShow[], GetTrendingShowRequest> {

    public GetTrendingShowRequest(int a_RequestId, IRequestStatus<TrendingShow[]> responseListener, Context a_context) {
        super(a_RequestId, responseListener, a_context);
    }

    @Override
    protected String getUrl() {
        return getHostName()+UrlConstants.PathElements.TRENDING_SHOW;
    }
}
