package com.mobileaware.telly.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sabri on 5/10/2016.
 */
public class TrendingShow implements Serializable {

    private int num;
    private String img;

    @SerializedName("lnk")
    private String link;

    private String name;
    private String air;
    private String desc;
    private List<ShowListing> listings;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAir() {
        return air;
    }

    public void setAir(String air) {
        this.air = air;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<ShowListing> getListings() {
        return listings;
    }

    public void setListings(List<ShowListing> listings) {
        this.listings = listings;
    }
}
