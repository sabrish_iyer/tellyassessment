package com.mobileaware.telly.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobileaware.telly.R;
import com.mobileaware.telly.Request.GetTrendingShowRequest;
import com.mobileaware.telly.Request.IRequestStatus;
import com.mobileaware.telly.Request.RequestResult;
import com.mobileaware.telly.pojo.TrendingShow;


/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class TrendingShowFragment extends Fragment implements View.OnClickListener {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private TrendingRecyclerViewAdapter trendingRecyclerViewAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TrendingShowFragment() {
    }

    public static TrendingShowFragment newInstance(int columnCount) {
        TrendingShowFragment fragment = new TrendingShowFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.trendind_fragment_item_list, container, false);

            view.findViewById(R.id.errorLayout).setOnClickListener(this);
        // Set the adapter
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            trendingRecyclerViewAdapter = new TrendingRecyclerViewAdapter(context,null, mListener);
            recyclerView.setAdapter(trendingRecyclerViewAdapter);
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getTrendingShow();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.errorLayout:
                getTrendingShow();
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(TrendingShow item);
    }

    private void getTrendingShow(){

        requestInProgress();
        IRequestStatus<TrendingShow[]> iRequestStatus  = new IRequestStatus<TrendingShow[]>() {

            @Override
            public void onRequestCompleted(RequestResult<TrendingShow[]> requestResult) {
                if(requestResult != null && requestResult.getRequestResponse() != null
                        && !requestResult.isRequestFailed()){
                   TrendingShow[] trendingShow = requestResult.getRequestResponse();
                   trendingRecyclerViewAdapter.setItems(trendingShow);
                   trendingRecyclerViewAdapter.notifyDataSetChanged();
                    responseSuccess();
                }else{
                    responseFailure();
                }

            }
        };

        GetTrendingShowRequest getTrendingShowRequest = new GetTrendingShowRequest(1,iRequestStatus,getContext());
        getTrendingShowRequest.execute();
    }

    private void responseSuccess(){
        getView().findViewById(R.id.progress).setVisibility(View.GONE);
        getView().findViewById(R.id.errorLayout).setVisibility(View.GONE);
        getView().findViewById(R.id.list).setVisibility(View.VISIBLE);
    }

    private void responseFailure(){
        getView().findViewById(R.id.progress).setVisibility(View.GONE);
        getView().findViewById(R.id.errorLayout).setVisibility(View.VISIBLE);
        getView().findViewById(R.id.list).setVisibility(View.GONE);
    }

    private void requestInProgress(){
        getView().findViewById(R.id.progress).setVisibility(View.VISIBLE);
        getView().findViewById(R.id.errorLayout).setVisibility(View.GONE);
        getView().findViewById(R.id.list).setVisibility(View.GONE);
    }
}
