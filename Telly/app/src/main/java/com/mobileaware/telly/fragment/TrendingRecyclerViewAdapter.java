package com.mobileaware.telly.fragment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.mobileaware.telly.R;
import com.mobileaware.telly.TellyApplication;
import com.mobileaware.telly.pojo.TrendingShow;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link TrendingShow} and makes a call to the
 * specified {@link com.mobileaware.telly.fragment.TrendingShowFragment.OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class TrendingRecyclerViewAdapter extends RecyclerView.Adapter<TrendingRecyclerViewAdapter.ViewHolder> {

    private  TrendingShow[] mValues;
    private final TrendingShowFragment.OnListFragmentInteractionListener mListener;
    private Context mContext;

    public TrendingRecyclerViewAdapter(Context context, TrendingShow[] items, TrendingShowFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        mContext =  context;
    }

    public void setItems(TrendingShow[] items){
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trending_fragment__item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues[position];
        holder.mShowName.setText(mValues[position].getName());
        holder.mContentView.setText(String.format(mContext.getResources().getString(R.string.airing_on), mValues[position].getAir()));
        holder.imageView.setImageUrl(mValues[position].getImg(), TellyApplication.getApplication().getImageLoader());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if(mValues == null){
            return 0;
        }
        return mValues.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mShowName;
        public final TextView mContentView;
        public final NetworkImageView imageView;
        public TrendingShow mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mShowName = (TextView) view.findViewById(R.id.showName);
            mContentView = (TextView) view.findViewById(R.id.content);
            imageView = (NetworkImageView)view.findViewById(R.id.showImage);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
