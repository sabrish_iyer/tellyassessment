package com.mobileaware.telly.Request;

public interface IRequestStatus<D> {
		public void onRequestCompleted(RequestResult<D> requestResult);
	}
	