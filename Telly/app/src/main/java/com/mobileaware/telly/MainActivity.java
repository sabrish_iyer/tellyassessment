package com.mobileaware.telly;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import com.mobileaware.telly.fragment.TrendingShowFragment;
import com.mobileaware.telly.pojo.TrendingShow;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, TrendingShowFragment.OnListFragmentInteractionListener {

    private float lastTranslate = 0.0f;
    private DrawerLayout mDrawerLayout;
    private int currentFragmentId = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

       final  NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);


        final CoordinatorLayout frame = (CoordinatorLayout) findViewById(R.id.content_frame);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout,toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        {
            @SuppressLint("NewApi")
            public void onDrawerSlide(View drawerView, float slideOffset)
            {
                float moveFactor = (navigationView.getWidth() * slideOffset);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                {
                    frame.setTranslationX(moveFactor);
                }
                else
                {
                    TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                    anim.setDuration(0);
                    anim.setFillAfter(true);
                    frame.startAnimation(anim);

                    lastTranslate = moveFactor;
                }
            }
        };

        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        setOnClickListener();
        setVersion();
        setFragment(R.id.trendingTextView);
    }

    private void setOnClickListener(){
        findViewById(R.id.trendingTextView).setOnClickListener(this);
        findViewById(R.id.fullScheduleTextView).setOnClickListener(this);
        findViewById(R.id.searchShowTextView).setOnClickListener(this);
        findViewById(R.id.favoritesTextView).setOnClickListener(this);
    }

    private void setVersion(){
        TextView versionView = (TextView)mDrawerLayout.findViewById(R.id.versiontextView);
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            versionView.setText(String.format(getResources().getString(R.string.version), version));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.trendingTextView:
                setFragment(v.getId());
                break;
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    private void setFragment(int id){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if(id != currentFragmentId) {
            ((TextView)toolbar.findViewById(R.id.toolbar_title)).setText(R.string.trendingShows);
            toolbar.setBackgroundColor(getResources().getColor(R.color.header_red));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.header_red));
            }
            currentFragmentId = id;
            Fragment fragment = null;
            if (id == R.id.trendingTextView) {
                fragment = TrendingShowFragment.newInstance(1);
            }
            if (fragment != null) {

                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_frame, fragment)
                        .commit();
            }
        }
    }

    @Override
    public void onListFragmentInteraction(TrendingShow item) {

    }
}
