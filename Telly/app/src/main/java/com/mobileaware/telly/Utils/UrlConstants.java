package com.mobileaware.telly.Utils;

/**
 * Created by sabri on 5/11/2016.
 */
public class UrlConstants {

    public static final String BASE_URL = "http://www.felipesilveira.com.br/";

    public class PathElements{

        public static final String TRENDING_SHOW = "tvguide/showtrends.php";
    }
}
