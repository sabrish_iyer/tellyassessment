package com.mobileaware.telly.Request;

public enum HTTPScheme {
	HTTP("http"),
	HTTPS("https")

	;

	private String m_scheme;

	private HTTPScheme(String scheme) {
		m_scheme = scheme;
	}

	public String getScheme() {
		return m_scheme;
	}

}