package com.mobileaware.telly.pojo;

import java.io.Serializable;

/**
 * Created by sabri on 5/10/2016.
 */
public class ShowListing implements Serializable {

    private String day;
    private String hour;
    private String title;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
