package com.mobileaware.telly.Request;

public enum RequestStatus {
		STARTED, PROGRESS, FINISHED
	}