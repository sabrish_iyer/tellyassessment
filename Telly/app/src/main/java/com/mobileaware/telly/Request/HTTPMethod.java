package com.mobileaware.telly.Request;

public enum HTTPMethod {
	GET,
	PUT,
	POST,
	DELETE,
	HEAD,
	OPTIONS,
	TRACE,
	CONNECT
}
