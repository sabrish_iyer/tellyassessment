package com.mobileaware.telly.Request;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.Map;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.google.gson.JsonDeserializer;
import com.mobileaware.telly.TellyApplication;
import com.mobileaware.telly.Utils.TellyUtils;
import com.mobileaware.telly.Utils.UrlConstants;

/**
 * @author sabrish
 *
 */
public class BaseRequest<T, Y extends BaseRequest<T, Y>>  implements Listener<T> , ErrorListener
{

	private static final String TAG = "BaseRequest - ";
	
	private IRequestStatus<T>     mResponseListener;
	private final Class<T> responseClass;
	private int mRequestId = -1;
	private RequestResult<T> mRequestResult;
	private JsonDeserializer<T> mJsonDeserializer;
	protected Context mContext;

	public BaseRequest(int a_RequestId, IRequestStatus<T> responseListener, Context a_context )
	{
		mRequestId = a_RequestId;
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		Type[] actualTypeArguments = genericSuperclass.getActualTypeArguments();
		try {
			responseClass = findActualClassArgument(actualTypeArguments[0]);
		} catch (RuntimeException e) {
			e.printStackTrace();
			throw e;
		}

		mRequestResult = new RequestResult<T>(a_RequestId);
		mResponseListener = responseListener;

		mContext = a_context;
	}
	

	private static <Z> Class<Z> findActualClassArgument(Type actualTypeArgument) {
		// if the actual type itself is parameterized, get it's raw type, otherwise just cast
		if (actualTypeArgument instanceof ParameterizedType) {
			ParameterizedType parameterizedType = (ParameterizedType) actualTypeArgument;
			return (Class<Z>) parameterizedType.getRawType();
		} else if (actualTypeArgument instanceof GenericArrayType) {
			GenericArrayType arrayType = (GenericArrayType) actualTypeArgument;
			return (Class<Z>) Array.newInstance((Class<?>) arrayType.getGenericComponentType(), 0).getClass();
		} else {
			return (Class<Z>) actualTypeArgument;
		}
	}
	
	public JsonDeserializer<T> getJsonDeserializer()
	{
		return mJsonDeserializer;
	}

	public void setJsonDeserializer(JsonDeserializer<T> mJsonDeserializer)
	{
		this.mJsonDeserializer = mJsonDeserializer;
	}


	protected String getUrl()
	{
		return null;
	}

	public void  execute()  {

		mRequestResult.setRequestStatus(RequestStatus.STARTED);
		String url = getUrl();
		_executeForURL(url);
	}


	protected void _executeForURL(String url) {
			Log.d(TellyUtils.TAG_APP, TAG + "_executeForURL  \n " + url);
		GsonRequest<T> gsonRequest = null;
		
		switch (getHTTPMethod()) {
			case GET:
				gsonRequest = new GsonRequest(Method.GET,getUrl(), getResponseClass(), getHeaders(), this, this);
				break;
			case POST:
				gsonRequest = new GsonRequest(Method.POST,getUrl(), getResponseClass(), getHeaders(), this, this);
				break;
			case PUT:
				break;
			case DELETE:
				break;
			default:
				throw new UnsupportedOperationException(getHTTPMethod().name() + " is not supported.");
		}
		
		if(gsonRequest != null)
		{
			if(mRequestId != -1)
			{
				gsonRequest.setTag(mRequestId);
			}
			
			gsonRequest.setJsonDeserializer(mJsonDeserializer);

			TellyApplication.getApplication().addToRequestQueue(gsonRequest);

		}
		
	}

	protected Class<T> getResponseClass()
	{
		return responseClass;
	}

	protected HTTPMethod getHTTPMethod()
	{
		return HTTPMethod.GET;
	}

	protected HTTPScheme getHttpScheme()
	{

		return HTTPScheme.HTTP;
	}

	protected String getHostName()
	{
		return UrlConstants.BASE_URL;
	}

	protected  Map<String, String> getHeaders()
	{
		return null;
	}

	@Override
	public void onResponse(T response) {

		if(response!= null )
		{
			Log.d(TellyUtils.TAG_APP, TAG + " onResponse" + " Request - "+getClass().getSimpleName());
			sendSucessResponse(response);
		}
		else
		{
			sendFailResponse(response);
		}

	}

	private void sendFailResponse(T response)
	{
		Log.d(TellyUtils.TAG_APP, TAG + " sendFailResponse" + " Request - "+getClass().getSimpleName());
		VolleyError error = new VolleyError();
		onErrorResponse(error);
	}

	private void sendSucessResponse(T response)
	{
		 Log.d(TellyUtils.TAG_APP, TAG + " sendSucessResponse" + " Request - " + getClass().getSimpleName());

		mRequestResult.setRequestStatus(RequestStatus.FINISHED);
		mRequestResult.setRequestResponse(response);
		mRequestResult.setRequestFailed(false);

		if(mResponseListener != null)
		{
			mResponseListener.onRequestCompleted(mRequestResult);;
		}
	}

	@Override
	public void onErrorResponse(VolleyError error) {

		mRequestResult.setRequestStatus(RequestStatus.FINISHED);
		mRequestResult.setRequestFailed(true);
		if(error != null)
		{
			mRequestResult.setErrorMessage(error.getMessage());
			mRequestResult.setError(error);
		}

		if(mResponseListener != null)
		{
			mResponseListener.onRequestCompleted(mRequestResult);;
		}
	}
}
